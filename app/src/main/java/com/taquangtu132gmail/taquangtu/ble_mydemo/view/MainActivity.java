package com.taquangtu132gmail.taquangtu.ble_mydemo.view;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.taquangtu132gmail.taquangtu.ble_mydemo.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button btnDelete;
    private Button btnScan;
    private Button btnStop;
    private ListView lvDevices;
    private DeviceAdapter deviceAdapter;
    private ArrayList<BluetoothDevice> bluetoothDeviceArrayList;
    private BluetoothAdapter bluetoothAdapter;
    private Handler myHandler;
    private BluetoothAdapter.LeScanCallback scanCallBack;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapViews();
        getBluetoothAdapter();
        setViewOnClick();
        initAdapter();
        initHandler();
        initScanCallBack();
    }
    private void mapViews()
    {
        btnDelete = findViewById(R.id.btn_delete);
        btnScan   = findViewById(R.id.btn_scan);
        btnStop   = findViewById(R.id.btn_stop);
        lvDevices = findViewById(R.id.lv_devices);
        progressBar = findViewById(R.id.progressBar);
    }
    private void setViewOnClick()
    {
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                long DELEY_TIME = 10000L; //10s
                myHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.INVISIBLE);
                        bluetoothAdapter.stopLeScan(scanCallBack);
                        deviceAdapter.notifyDataSetChanged();
                    }
                }, DELEY_TIME);

                //clear all devices before scanning
                bluetoothDeviceArrayList.clear();
                deviceAdapter.notifyDataSetChanged();
                //then scan
                bluetoothAdapter.startLeScan(scanCallBack);
                deviceAdapter.notifyDataSetChanged();
            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bluetoothAdapter.stopLeScan(scanCallBack);
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
        lvDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Intent intent = new Intent(MainActivity.this, DeviceInformationActivity.class);
                BluetoothDevice device = bluetoothDeviceArrayList.get(i);
                intent.putExtra("REMOTE", device);
                MainActivity.this.startActivity(intent);
            }
        });
    }
    private void getBluetoothAdapter()
    {
        bluetoothAdapter = ((BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        if(!bluetoothAdapter.isEnabled())
        {
            Intent requestBT = new Intent(bluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(requestBT, 1);
        }
    }
    private void initAdapter()
    {
        bluetoothDeviceArrayList = new ArrayList<>();
        deviceAdapter = new DeviceAdapter(this, R.layout.row_device, bluetoothDeviceArrayList);
        lvDevices.setAdapter(deviceAdapter);
    }
    private void initHandler()
    {
        myHandler = new Handler();
    }
    private void initScanCallBack()
    {
        scanCallBack = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(final BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
                 runOnUiThread(new Runnable() {
                     @Override
                     public void run() {
                         if(!bluetoothDeviceArrayList.contains(bluetoothDevice))
                         {
                             bluetoothDeviceArrayList.add(bluetoothDevice);

                         }
                         deviceAdapter.notifyDataSetChanged();
                     }
                 });
            }
        };
    }
}

package com.taquangtu132gmail.taquangtu.ble_mydemo.model;

import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class CharacteristicsWrapper implements Serializable
{

    ArrayList<BluetoothGattCharacteristic> characteristics;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public CharacteristicsWrapper createFromParcel(Parcel in) {
            return new CharacteristicsWrapper(in);
        }

        public CharacteristicsWrapper[] newArray(int size) {
            return new CharacteristicsWrapper[size];
        }
    };

    public CharacteristicsWrapper(Parcel in) {

    }

    public CharacteristicsWrapper(ArrayList<BluetoothGattCharacteristic> characteristics) {
        this.characteristics = characteristics;
    }

    public ArrayList<BluetoothGattCharacteristic> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(ArrayList<BluetoothGattCharacteristic> characteristics) {
        this.characteristics = characteristics;
    }
}

package com.taquangtu132gmail.taquangtu.ble_mydemo.view;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.taquangtu132gmail.taquangtu.ble_mydemo.R;

import java.util.ArrayList;

public class DeviceInformationActivity extends AppCompatActivity {
    private TextView tvDeviceName;
    private TextView tvDeviceUIID;
    private ListView lvServices;
    private BluetoothGatt bluetoothGatt; //get instance from intent from MainActivity
    private BluetoothGattCallback gattCallback;
    private ArrayList<BluetoothGattService> gattServiceArrayList;
    private ArrayList<String> serviceUUIDArrayList;
    private ArrayAdapter<String> serviceUUIDArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_information);
        mapViews();
        setAdapter(); //set services list view adapter
        setGattCallback();
        setBluetoothGatt();
        setOnListViewItemClick();
    }
    public void mapViews()
    {
        this.lvServices = findViewById(R.id.lv_services);
        this.tvDeviceName = findViewById(R.id.tv_device_name);
        this.tvDeviceUIID = findViewById(R.id.tv_device_uuid);
    }
    void setBluetoothGatt()
    {
        BluetoothDevice device = (BluetoothDevice) getIntent().getExtras().getParcelable("REMOTE");
        bluetoothGatt = device.connectGatt(this, false, gattCallback);
        //show device name and its uuid
        tvDeviceName.setText("Device name: "+device.getName().toString());
        tvDeviceUIID.setText("MAC: "+device.getAddress().toString());
    }
    void setGattCallback()
    {
        gattCallback = new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, final int newState)
            {
                final int _newState = newState;
                final BluetoothGatt _gatt = gatt;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(_newState == 2) //connected
                        {
                            _gatt.discoverServices();
                            serviceUUIDArrayAdapter.notifyDataSetChanged();
                        }
                        else if(newState == 0) //disconnected, clear all old service
                        {
                            gattServiceArrayList.clear();
                            serviceUUIDArrayList.clear();
                            serviceUUIDArrayAdapter.notifyDataSetChanged();
                        }
                    }
                });
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    if(status == 0) // BluetoothGatt.GATT_SUCCESS
                    {
                        gattServiceArrayList = (ArrayList<BluetoothGattService>) gatt.getServices();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run()
                            {
                                serviceUUIDArrayList.clear();
                                for(int i = 0;i < gattServiceArrayList.size();i++)
                                {
                                    serviceUUIDArrayList.add(gattServiceArrayList.get(i).getUuid().toString());
                                }
                                serviceUUIDArrayAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                    else
                        {
                            Log.d("DeviceInformationAct", "onServicesDiscovered: status = GATT_FAIL");
                        }
            }

            //no need to override this function because we are just finding services
            @Override
            public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            }
            //no need to override this function because we are just finding services
            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicChanged(gatt, characteristic);
            }
        };
    }
    public void setAdapter()
    {
        serviceUUIDArrayList = new ArrayList<>();
        this.serviceUUIDArrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, serviceUUIDArrayList);
        if(serviceUUIDArrayAdapter==null)
        {
            Log.d("x", "setAdapter: serviceUUIDArrayAdapter ==null");
            return;
        }
        lvServices.setAdapter(serviceUUIDArrayAdapter);
    }
    public void setOnListViewItemClick()
    {
        lvServices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(DeviceInformationActivity.this, CharacteristicActivity.class);
                intent.putExtra("characteristicsList",(ArrayList<BluetoothGattCharacteristic>)bluetoothGatt.getServices().get(i).getCharacteristics());
                DeviceInformationActivity.this.startActivity(intent);
            }
        });
    }
}

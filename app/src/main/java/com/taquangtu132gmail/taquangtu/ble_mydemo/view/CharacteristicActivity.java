package com.taquangtu132gmail.taquangtu.ble_mydemo.view;

import android.bluetooth.BluetoothGattCharacteristic;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.taquangtu132gmail.taquangtu.ble_mydemo.R;

import java.util.ArrayList;

public class CharacteristicActivity extends AppCompatActivity {

    private TextView tvValue;
    private TextView tvPermission;
    private ListView lvCharacteristics;
    private ArrayList<BluetoothGattCharacteristic> characteristicArrayList;
    private ArrayList<String> characteristicUUIDArrayList;
    private ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characteristic);
        mapViews();
        setArrayAdapter();
        setGattService();
        setOnListViewItemClick();
    }
    public void mapViews()
    {
        tvValue =  findViewById(R.id.tv_character_value);
        tvPermission = findViewById(R.id.tv_character_permission);
        lvCharacteristics =findViewById(R.id.lv_characteristics);
    }
    public void setArrayAdapter()
    {
        characteristicUUIDArrayList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,characteristicUUIDArrayList);
        lvCharacteristics.setAdapter(arrayAdapter);
    }
    public void setGattService()
    {
        characteristicArrayList = getIntent().getExtras().getParcelable("characteristicsList");
        characteristicUUIDArrayList.clear();
        if(characteristicArrayList==null||characteristicArrayList.size()==0)
        {
            tvValue.setText("No characteristic this service");
            return;
        }
        for(int i = 0;i<characteristicArrayList.size();i++)
        {
            characteristicUUIDArrayList.add(characteristicArrayList.get(i).getUuid().toString());
        }
        arrayAdapter.notifyDataSetChanged();
    }
    public void setOnListViewItemClick()
    {
        lvCharacteristics.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BluetoothGattCharacteristic characteristic = characteristicArrayList.get(i);
                final byte[] data = characteristic.getValue();
                if (data != null && data.length > 0)
                {
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for(byte byteChar : data)
                    {
                        stringBuilder.append(String.format("%02X ", byteChar));
                    }
                    tvValue.setText(stringBuilder);
                }
                switch (characteristic.getPermissions())
                {
                    case 1: tvPermission.setText("READ"); break;
                    case 2: tvPermission.setText("READ_ENCRYPTED"); break;
                    case 4: tvPermission.setText("READ_ENCRYPTED_MITM"); break;
                    case 16: tvPermission.setText("WRITE"); break;
                    case 32: tvPermission.setText("WRITE_ENCRYPTED"); break;
                    case 64: tvPermission.setText("WRITE_ENCRYPTED_MITM"); break;
                    case 128: tvPermission.setText("WRITE_SIGNED"); break;
                    case 256: tvPermission.setText("WRITE_SIGNED_MITM"); break;
                }
            }
        });
    }
}

package com.taquangtu132gmail.taquangtu.ble_mydemo.view;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.taquangtu132gmail.taquangtu.ble_mydemo.R;

import java.util.List;

public class DeviceAdapter extends BaseAdapter {
    private Context context;
    private int layoutResourceID;
    private List<BluetoothDevice> bluetoothDeviceList;

    public DeviceAdapter(Context context, int layoutResourceID, List<BluetoothDevice> bluetoothDeviceList) {
        this.context = context;
        this.layoutResourceID = layoutResourceID;
        this.bluetoothDeviceList = bluetoothDeviceList;
    }

    @Override
    public int getCount() {
        return bluetoothDeviceList.size();
    }

    @Override
    public Object getItem(int i) {
        return bluetoothDeviceList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder = new ViewHolder();
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layoutResourceID, null);
            viewHolder.tvDeviceName = view.findViewById(R.id.tv_device_name);
            viewHolder.tvDeviceMacAddress = view.findViewById(R.id.tv_device_mac_address);
            viewHolder.tvStatus = view.findViewById(R.id.tv_value);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        if (bluetoothDeviceList.get(i).getName() == null || bluetoothDeviceList.get(i).getName().equals("")) {
            viewHolder.tvDeviceName.setText("Unknown name");
        } else {
            viewHolder.tvDeviceName.setText(bluetoothDeviceList.get(i).getName());
        }
        viewHolder.tvDeviceMacAddress.setText(bluetoothDeviceList.get(i).getAddress());
        int state = bluetoothDeviceList.get(i).getBondState();
        switch (state) {
            case 10:
                viewHolder.tvStatus.setText("Not paired");
                break;
            case 11:
                viewHolder.tvStatus.setText("Pairing");
                break;
            case 12:
                viewHolder.tvStatus.setText("paired");
                break;
        }

        return view;
    }

    class ViewHolder {
        TextView tvDeviceName;
        TextView tvDeviceMacAddress;
        TextView tvStatus;
    }
}
